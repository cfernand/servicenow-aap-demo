# Ansible Collection - ansible_ssa.windows

Documentation for the collection.

This collection contains the following roles. Please read the respective role documentation for more details.

|           Role         |                    Description                  |
|:----------------------:|:-----------------------------------------------:|
|       dc_setup         | Deploy Windows Domain Controller                |
|       dc_join          | Join Windows Domain Controller                  |
|      win_update        | Install Windows Updates                         |
|      win_report        | Get Windows Updates Report                      |
|     win_firewall       | Config Windows firewall                         |
|      wsus_setup        | Deploy Windows Server Update Services           |
|    wsus_configure      | Configure Windows Server Update Services        |
|   wsus_auto_approve    | Auto-Approve Windows Server Update Services     |
|      wsus_gpo          | Create Windows Server Update Services GPO       |
|      wsus_join         | Join Windows Server Update Services             |
|   wsus_pull_report     | Pull report from Windows Server Update Services |
|      iis_setup         | Deploy Internet Information Services            |
