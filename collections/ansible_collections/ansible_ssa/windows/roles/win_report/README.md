# Ansible Role: win_report

This role will help to send report with Windows Updates.

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Send report with Windows Updates
  var:
    win_update_category_names:
      - CriticalUpdates
      - DefinitionUpdates
      - SecurityUpdates
      - UpdateRollups
      - Updates
    sendgrid_to_email_addresses:
      - "{{ sendgrid_from_email_address }}"
    send_email_with_smtp: no
    save_report_locally: no
    smtp_server_host: smtp.gmail.com
    smtp_server_port: 587
    smtp_account: ''
    smtp_account_password: ''
    smtp_to_email_addresses: []
      ansible.builtin.include_role:
    name: ansible_ssa.windows.win_report
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
