# Ansible Role: dc_join

This role will help to join a Windows Client to a Windows Domain Controller. 

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
  - name: azure.azcollection
```

If you want to use this role in your playbook:

```yaml
- name: Join Windows Client to a Domain
  var:
    pdc_administrator_username: Administrator@redhat.local
    pdc_administrator_password: PassW0rd!
    pdc_dns_nics: "*"
    pdc_domain: redhat.local
    pdc_ou_client_paths:
      - OU=WindowsServers,DC=redhat,DC=local
    pdc_ou_wsus_paths:
      - OU=WSUSServers,DC=redhat,DC=local
    client_vm_servers: []
    wsus_vm_servers: []
  ansible.builtin.include_role:
    name: ansible_ssa.windows.dc_join
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
