# Ansible Role: iis_setup

This role will help to deploy a Windows Information Internet Services.

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Install Windows Information Internet Services
  var:
    ansible_site_path: "c:\\inetpub\\wwwroot\\ansibletest"
    staging_path: "c:\\deploy"
    ansible_test_staging_path: "{{ staging_path }}\\ansible-test-site-{{ ansible_date_time.year }}{{ ansible_date_time.month }}{{ ansible_date_time.day }}"
  ansible.builtin.include_role:
    name: ansible_ssa.windows.iis_setup
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
