# Ansible Role: dc_setup

This role will help to deploy a Windows Domain Controller. 

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Deploy Windows Domain Controller
  var:
    ansible_windows_domain_member: ""
    pdc_administrator_username: ansible
    pdc_administrator_password: PassW0rd!
    pdc_dns_nics: "*"
    pdc_dns_servers: "{{ ansible_host }}"
    pdc_domain: redhat.local
    pdc_netbios: REDHAT
    pdc_domain_path: DC=redhat,DC=local
    pdc_ou_names: 
      - WindowsServers
      - WSUSServers
    pdc_domain_safe_mode_password: PassW0rd!
    pdc_domain_functional_level: Default
    pdc_forest_functional_level: Default
    pdc_required_psmodules: [xPSDesiredStateConfiguration, NetworkingDsc, ComputerManagementDsc, ActiveDirectoryDsc] 
    pdc_required_features: ["AD-domain-services", "DNS"]
    pdc_desired_dns_forwarders: ["8.8.8.8", "8.8.4.4"]
  ansible.builtin.include_role:
    name: ansible_ssa.windows.dc_setup
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
