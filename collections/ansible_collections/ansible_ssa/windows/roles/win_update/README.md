# Ansible Role: win_update

This role will help to patch all Windows Updates with automatic reboots.

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Install all updates with automatic reboots
  var:
    win_update_blacklist: []
    win_update_whitelist: []
    win_update_category_names:
      - CriticalUpdates
      - DefinitionUpdates
      - SecurityUpdates
      - UpdateRollups
      - Updates
  ansible.builtin.include_role:
    name: ansible_ssa.windows.win_update
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
