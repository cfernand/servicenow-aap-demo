# Ansible Role: wsus_configure

This role will help to configure Windows Server Update Services.

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Configure Windows Server Update Services
  var:
    wsus_content_dir: C:\WSUS
    set_wsus_max_memory: yes
    set_wsus_max_xml_per_request_to_unlimited: yes
    wsus_restart_private_memory_limit: 4000000
  ansible.builtin.include_role:
    name: ansible_ssa.windows.wsus_configure
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
