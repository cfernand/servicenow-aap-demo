---
- name: unregister {{ instance_name }} from RHN if reachable
  ansible.builtin.command:
    cmd: 'ssh -o "StrictHostKeyChecking=off" ec2-user@{{ instance_name }}.{{ dns_suffix }}.ansible-labs.de "sudo subscription-manager unregister"'
  when:
    - instance_name is defined
    - dns_suffix is defined
  ignore_errors: true

- name: remove DNS record
  include_role:
    name: nsupdate
  vars:
    shortname: "{{ instance_name }}"
    mode: remove
  when:
    - dns_update | bool

- name: gather VPC information
  amazon.aws.ec2_vpc_net_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: vpc

- name: gather subnet information
  amazon.aws.ec2_vpc_subnet_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: subnet

- name: remove EC2 {{ ec2_os_type }} instance {{ instance_name }} - this can take a minute
  community.aws.ec2_instance:
    name: "{{ instance_name }}"
    region: "{{ ec2_region }}"
    vpc_subnet_id: "{{ subnet.subnets[0].subnet_id }}"
    state: absent
  when:
    - subnet.subnets[0].subnet_id is defined

- name: get instances for this VPC
  amazon.aws.ec2_instance_info:
    filters:
      "vpc-id": "{{ vpc.vpcs[0].vpc_id }}"
    region: "{{ ec2_region }}"
  register: vpc_instance_list
  when:
    - vpc.vpcs != []

- name: initialize variable vpc_emtpy
  set_fact:
    vpc_empty: false

- name: check if VPC does not exist or there are no instances
  set_fact:
    vpc_empty: true
  when: vpc.vpcs == [] or vpc_instance_list.instances == [] | default('[]')

- name: remove SSH Key pair
  amazon.aws.ec2_key:
    name: "{{ ec2_vpc_name }}"
    state: absent
    region: "{{ ec2_region }}"
  when:
    - vpc_empty | bool

- name: remove EC2 security group {{ instance_name }}
  amazon.aws.ec2_group:
    name: "{{ instance_name }}"
    region: "{{ ec2_region }}"
    state: absent

- name: remove Subnet
  amazon.aws.ec2_vpc_subnet:
    vpc_id: "{{ subnet.subnets[0].vpc_id }}"
    state: absent
    cidr: "172.31.1.0/24"
    region: "{{ ec2_region }}"
  when:
    - subnet.subnets[0].subnet_id is defined
    - vpc_empty | bool

- name: gather Route Table information
  community.aws.ec2_vpc_route_table_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: route_table

- name: remove Route Table
  community.aws.ec2_vpc_route_table:
    route_table_id: "{{ route_table.route_tables[0].id }}"
    vpc_id: "{{ vpc.vpcs[0].vpc_id }}"
    state: absent
    region: "{{ ec2_region }}"
    lookup: id
  when:
    - route_table.route_tables[0].id is defined
    - vpc_empty | bool

- name: gather internet gateway information
  community.aws.ec2_vpc_igw_info:
    filters:
      "tag:Name": "{{ ec2_vpc_name }}"
    region: "{{ ec2_region }}"
  register: igw

- name: remove internet gateway
  community.aws.ec2_vpc_igw:
    state: absent
    vpc_id: "{{ vpc.vpcs[0].vpc_id }}"
    region: "{{ ec2_region }}"
  when:
    - igw.internet_gateways[0].internet_gateway_id is defined
    - vpc_empty | bool

- name: remove VPC {{ ec2_vpc_name }} if there are no instances left
  amazon.aws.ec2_vpc_net:
    name: "{{ ec2_vpc_name }}"
    cidr_block: "172.31.0.0/16"
    state: absent
    region: "{{ ec2_region }}"
  when:
    - vpc_empty | bool
