---
- name: create {{ instance_name }} disk
  google.cloud.gcp_compute_disk:
      name: "{{ instance_name }}-disk"
      size_gb: "{{ instance_disk_size }}"
      source_image: "{{ gcp_template }}"
      state: present
      zone: "{{ gcp_zone }}"
  register: disk

- name: create {{ instance_name }} network
  google.cloud.gcp_compute_network:
      name: "{{ instance_name }}-network"
      state: present
      auto_create_subnetworks: true
  register: network

- name: create {{ instance_name }} address
  google.cloud.gcp_compute_address:
      name: "{{ instance_name }}-address"
      state: present
      region: "{{ gcp_region }}"
  register: address

- name: open network ports for Windows
  set_fact:
    port_list:
      - "80"
      - "443"
      - "3389"
      - "5986"
  when: gcp_template is search ("indows")

- name: set port lists for Linux
  set_fact:
    port_list:
      - "22"
      - "80"
      - "443"
  when: not gcp_template is search ("indows")

- name: expand port list with additional port
  ansible.builtin.set_fact:
    port_list: "{{ port_list + [ instance_additional_port ] }}"
  when: instance_additional_port is defined

- name: allow instance incoming traffic
  google.cloud.gcp_compute_firewall:
    name: "{{ instance_name }}-firewall"
    state: present
    allowed:
    - ip_protocol: tcp
      ports: "{{ port_list }}"
    network: "{{ network }}"

- name: create {{ instance_name }} instance
  google.cloud.gcp_compute_instance:
    state: present
    name: "{{ instance_name }}"
    machine_type: "{{ instance_flavor }}"
    metadata:
      sysprep-specialize-script-url: https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1
    disks:
    - auto_delete: true
      boot: true
      source: "{{ disk }}"
    network_interfaces:
    - network: "{{ network }}"
      access_configs:
        - name: 'External NAT'
          nat_ip: "{{ address }}"
          type: 'ONE_TO_ONE_NAT'
    zone: "{{ gcp_zone }}"
  register: instance

- name: store VM facts
  ansible.builtin.set_fact:
    vm: "{{ instance }}"

- name: print instance details
  ansible.builtin.debug:
    var: vm

- name: aggregating stats for the new VM
  ansible.builtin.set_stats:
    data:
      vm_puclic_ip_address: "{{ vm.networkInterfaces[0].accessConfigs[0].natIP }}"

- name: update DNS record
  include_role:
    name: nsupdate
  vars:
    ipaddress: "{{ address.address }}"
    shortname: "{{ instance_name }}"
    mode: update
  when: dns_update | bool

- name: wait for instance to start
  wait_for:
    host: "{{ address.address }}"
    port: 22
    delay: 10
    timeout: 60
  when:
    - not gcp_template is search ("indows")

- name: add host to in memory inventory
  ansible.builtin.add_host:
    name: "{{ instance_name }}"
    ansible_host: "{{ instance_name }}.{{ dns_suffix }}.ansible-labs.de"
    ansible_user: ansible
    groups:
    - "{{ instance_group | default ('all_hosts') }}"
  when: dns_update | bool
