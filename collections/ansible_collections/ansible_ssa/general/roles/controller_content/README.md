# Ansible Role: controller_content

This role will configure an existing automation controller. It will setup content which can be used as a foundation for Ansible Automation Platform Demos or as a blueprint.

## Controller Content

### Inventories

The role will setup a AWS, Azure and GCP dynamic inventories depending on the "type" variable.

### Machine Credentials

The role will add an "ansible" machine credential which will be used to log into managed nodes during Playbook execution.

### GitLab Projects

The role will setup a set of GitLab repositories which are used by the Job Templates.

### Job Templates

A few examples to setup and remove a Web Server. Also a set of Job Templates to deploy an automation controller, install and configure it (in fact calling this role, which will allow easier development of future roles).

### Manifest

Will install and enable a user provided Ansible Automation Platform Manifest.

## How to install this role

The Ansible SSA collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use

The role is optimized to be used from the [Playbook RHAAP](https://gitlab.com/ansible-ssa/playbook-rhaap) which will be called by a Job Template created by this role.

If you want to use it in your playbook:

```yaml
  - name: Configure automation controller
    include_role:
      name: ansible.ssa.general.controller_content
```

Instead of using the role directly, consider using the [Ansible SSA Collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection)

## Variables

This role is using a list of variables to configure the automation controller. Check the [defaults](defaults/main.yml) and adjust them to your needs.
